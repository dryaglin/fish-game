const FISH_POSITION = {
  TOP: 'top',
  BOTTOM: 'bottom',
  MIDDLE: 'middle'
}

const FISH_ONE = {
  name: 'Щука',
  position: FISH_POSITION.TOP
}

const FISH_TWO = {
  name: 'Окунь',
  position: FISH_POSITION.TOP
}

const FISH_THREE = {
  name: 'Плiтка',
  position: FISH_POSITION.TOP
}

const FISH_FOUR = {
  name: 'Короп',
  position: FISH_POSITION.BOTTOM
}

const FISH_FIVE = {
  name: 'Лящ',
  position: FISH_POSITION.BOTTOM
}

const FISH_SIX = {
  name: 'Сом',
  position: FISH_POSITION.BOTTOM
}

const FISH_SEVEN = {
  name: '',
  position: FISH_POSITION.MIDDLE,
  texture: 'assets/fish_group_seven.png'
}

const FISH_DATA = [
  FISH_ONE,
  FISH_SIX,
  FISH_TWO,
  FISH_FOUR,
  FISH_FIVE,
  FISH_ONE,
  FISH_FIVE,
  FISH_ONE,
  FISH_THREE,
  FISH_THREE,
  FISH_SIX,
  FISH_FOUR,
  FISH_TWO,
  FISH_SEVEN
];

export {
  FISH_POSITION,
  FISH_ONE,
  FISH_TWO,
  FISH_THREE,
  FISH_FOUR,
  FISH_FIVE,
  FISH_SIX
}

export default FISH_DATA;
