const ASSETS = [
  'assets/under_water.jpg',
  'assets/golden_fishes.png',
  'assets/school-of-fish.png',
  'assets/simple.png',
  'assets/bubble.png',
  'assets/fish-7.png',
  'assets/fish_group_seven.png',
  'assets/wave-41.mp3',
];

export default ASSETS;
