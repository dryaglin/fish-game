import * as PIXI from 'pixi.js';

class AssetsLoader {
  constructor () {
    this.loader = PIXI.Loader.shared
  }

  async load (assets) {
    return new Promise((resolve, reject) => {
      this.loader.add(assets)
        .load(() => {
          resolve()
        });
    })
  }

  getTexture (name) {
    return this.loader.resources[`${name}`].texture;
  }

  getAmbientSound () {
    return this.loader.resources['assets/wave-41.mp3'];
  }
}

const assetsLoader = new AssetsLoader();

export default assetsLoader;
