import * as PIXI from 'pixi.js';
import assetsLoader from '../../model/assets-loader';
import { gsap } from "gsap";
import main from '../../main';
import { getRandomInt } from '../../utils/utils';

class Background extends PIXI.Container {
  constructor (texture) {
    super()

    this.backImage = this.addChild(new PIXI.Sprite(texture));

    this.bubblesLeft = this.addChild(new PIXI.Container());
    this.bubblesLeft.x = 0;
    this.bubblesLeft.y = 0;
    this.bubblesLeft.width = 200;
    this.bubblesLeft.height = 600;

    this.bubblesRight = this.addChild(new PIXI.Container());
    this.bubblesRight.x = 600;
    this.bubblesRight.y = 0;
    this.bubblesRight.width = 200;
    this.bubblesRight.height = 600;


    main.app.ticker.add((delta) => {
      if (this.bubblesLeft.children.length === 0) {
        this.addBubblesLeft();
      }
      if (this.bubblesRight.children.length === 0) {
        this.addBubblesRight();
      }
    });
  }

  addBubblesLeft () {
    const count = getRandomInt(10, 20);
    for (let i = 0; i < count; i++) {
      const bubble = this.getBubble(this.bubblesLeft, 135, 530);
      setTimeout(() => this.animateBubble(bubble, this.bubblesLeft), getRandomInt(2000, 5000));
    }
  }

  addBubblesRight () {
    const count = getRandomInt(10, 20);
    for (let i = 0; i < count; i++) {
      const bubble = this.getBubble(this.bubblesRight, 0, 420);
      setTimeout(() => this.animateBubble(bubble, this.bubblesRight), getRandomInt(2000, 5000));
    }
  }

  animateBubble (bubble, container) {
    gsap.to(bubble, {
      alpha: 1,
      duration: 1
    });

    gsap.to(bubble, {
      y: -bubble.height,
      x: getRandomInt(50, 150),
      duration: getRandomInt(10, 15),
      onComplete: () => {
        container.removeChild(bubble);
      },
      ease: 'sine.inOut'
    });
  }

  getBubble (container, x, y) {
    const bubble = container.addChild(new PIXI.Sprite(assetsLoader.getTexture('assets/bubble.png')));

    bubble.anchor.set(0.5);
    bubble.scale.set(0.2);
    bubble.x = x;
    bubble.y = y;
    bubble.alpha = 0;

    return bubble;
  }

  resize (w, h) {

  }
}

export default Background
