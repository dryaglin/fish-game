import * as PIXI from 'pixi.js';
import main from '../../main';
import assetsLoader from '../../model/assets-loader';
import { gsap } from "gsap";
import { FISH_NAME_STYLE } from '../../style/base-text-style';
import { FISH_POSITION } from '../../model/fish-data';
import { getRandomInt } from '../../utils/utils';

class SimpleFish extends PIXI.Container {
  constructor ({ name, position }, texture) {
    super();

    this.rootView = main.app.renderer.view;

    this._scale = 0.5;
    this._scaleMin = 0.2;
    this._name = name;
    this._swimPosition = position;

    this.fishImage = this.addChild(new PIXI.Sprite(assetsLoader.getTexture(texture || 'assets/simple.png')));
    this.fishImage.anchor.set(0.5);
    this.fishImage.scale.set(this._scale, this._scale)

    this.x = -(this.fishImage.width);
    this.y = this.rootView.height * 0.5;

    this.fishName = this.addChild(new PIXI.Text(this._name, FISH_NAME_STYLE));
    this.fishName.anchor.set(0.5);
    this.fishName.scale.set(this._scale, this._scale)
    this.fishName.x = 0;
    this.fishName.y = 0;
  }

  swim () {
    return new Promise(resolve => {
      gsap.to(this, {
        x: this.rootView.width * 0.5,
        duration: 2,
        onComplete: () => {
          resolve();
        }
      });
    })

  }

  swimToPosition (lockDirection = false) {
    return new Promise(resolve => {

      const size = 150;
      let targetPosX = getRandomInt(50, this.rootView.width - 50);

      targetPosX = (targetPosX < size) ? size : targetPosX;
      targetPosX = (targetPosX > this.rootView.width - size) ? this.rootView.width - size : targetPosX;

      const targetPosY = this._swimPosition === FISH_POSITION.TOP ? getRandomInt(50, 200) : getRandomInt(400, 550);
      const direction = (!lockDirection && targetPosX < this.rootView.width * 0.5) ? -1 : 1;

      gsap.to(this.fishImage.scale, { x: this._scaleMin * direction, y: this._scaleMin, duration: 0.5 });
      gsap.to(this.fishName.scale, { x: this._scaleMin, y: this._scaleMin, duration: 0.6 });
      gsap.to(this, {
        x: targetPosX,
        y: targetPosY,
        duration: 3,
        onComplete: () => {
          resolve();
          this.swimFree();
        }
      });
    })
  }

  swimFree () {
    setTimeout(() => {
      let targetPosX = getRandomInt(50, this.rootView.width - 50);
      const direction = (this.x > targetPosX) ? -1 : 1;

      this.swimFreeAnimate(22 + Math.random() * 2, targetPosX, direction)
    }, parseInt(Math.random() * 4000));
  }

  swimFreeAnimate (duration, x, direction) {
    gsap.to(this.fishImage.scale, { x: this._scaleMin * direction, duration: 0.5 });
    gsap.to(this, {
      x: x,
      duration: duration,
      onComplete: () => this.swimFree()
    });
  }

  get swimPosition () {
    return this._swimPosition;
  }

  stop () {
    gsap.killTweensOf(this.fishImage.scale);
    gsap.killTweensOf(this);
  }
}

export default SimpleFish
