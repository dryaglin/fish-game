import * as PIXI from 'pixi.js';
import { gsap } from "gsap";
import main from '../../main';

export class FishesGroup extends PIXI.Sprite {
  constructor (scale, texture) {
    super(texture);

    this.rootView = main.app.renderer.view;
    this._scale = scale;
    this.anchor.set(0.5);
    this.alpha = 1;
    this.scale.set(this._scale, this._scale)
    this.x = Math.random() > 0.5 ? this.rootView.width + this.width : -(this.width);
  }

  createFishesGroup () {
    setTimeout(() => {
      let targetX = 0;

      if (this.x < 0) {
        targetX = this.rootView.width + this.width;
        this.scale.set(this._scale, this._scale)
      } else if (this.x > this.rootView.width) {
        targetX = -(this.width);
        this.scale.set(-this._scale, this._scale)
      }

      this.y = Math.random() * this.rootView.height;

      this.animate(22 + Math.random() * 2, targetX)
    }, parseInt(Math.random() * 4000));
  }

  animate (duration, x) {

    gsap.to(this, {
      x: x,
      duration: duration,
      onComplete: () => this.createFishesGroup()
    });
  }
}
