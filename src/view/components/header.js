import * as PIXI from 'pixi.js';
import main from '../../main';
import { HEADER_TEXT } from '../../style/base-text-style';

class Header extends PIXI.Container {
  constructor () {
    super();

    this.successFishesText = 'Вiрно: ';
    this.successCounter = 0;
    this.wrongFishesText = 'Хибно: ';
    this.wrongCounter = 0;


    this.rootView = main.app.renderer.view;

    this.initView();
  }

  initView () {
    this.back = this.addChild(new PIXI.Graphics());
    this.back.beginFill(0x000000, 0.1);
    this.back.drawRect(0, 0, this.rootView.width, 30);
    this.back.endFill();

    this.headerLeftText = this.addChild(new PIXI.Text(`${this.successFishesText}${this.successCounter}`, HEADER_TEXT));
    this.headerLeftText.anchor.set(0.5, 1);
    this.headerLeftText.x = this.headerLeftText.width;
    this.headerLeftText.y = this.height * 0.5;

    this.headerRightText = this.addChild(new PIXI.Text(`${this.wrongFishesText}${this.wrongCounter}`, HEADER_TEXT));
    this.headerRightText.anchor.set(0.5, 1);
    this.headerRightText.x = this.rootView.width - this.headerRightText.width;
    this.headerRightText.y = this.height * 0.5;
  }

  setSuccess () {
    this.successCounter++;
    this.headerLeftText.text = `${this.successFishesText}${this.successCounter}`;
  }

  setWrong () {
    this.wrongCounter++;
    this.headerRightText.text = `${this.wrongFishesText}${this.wrongCounter}`;
  }

  reset () {
    this.successCounter = 0;
    this.headerLeftText.text = `${this.successFishesText}${this.successCounter}`;

    this.wrongCounter = 0;
    this.headerRightText.text = `${this.wrongFishesText}${this.wrongCounter}`;
  }

}

export default Header;
