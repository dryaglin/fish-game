import * as PIXI from 'pixi.js';
import { COUNTER_TEXT } from '../../style/base-text-style';

class Counter extends PIXI.Container {
  constructor () {
    super();

    this.back = this.addChild(new PIXI.Graphics());
    this.back.beginFill(0x000000, 0.5);
    this.back.drawRoundedRect(0, 0, 50, 50, 5);
    this.back.endFill();

    this.counterText = this.addChild(new PIXI.Text(`0`, COUNTER_TEXT));
    this.counterText.anchor.set(0.5);
    this.counterText.x = 25;
    this.counterText.y = 25;

    this.value = 0;
  }

  inc () {
    this.value++;
    this.counterText.text = this.value;
  }

  dec () {
    this.value = Math.min(0, this.value - 1);
    this.counterText.text = this.value;
  }
}

export default Counter;
