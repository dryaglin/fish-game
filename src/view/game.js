import * as PIXI from 'pixi.js';
import assetsLoader from '../model/assets-loader';
import { FishesGroup } from './components/fishes-group';
import Background from './components/background';
import main from '../main';
import { BASE_STYLE } from '../style/base-text-style';
import GameController, { GAME_STATE } from '../gameController';
import Header from './components/header';
import Counter from './components/counter';

class Game extends PIXI.Container {
  constructor () {
    super()

    this.interactive = true;

    this.rootView = main.app.renderer.view;
    this.gameController = new GameController(this);

    this.initGameHandler();
    this.initView();
  }

  initView () {
    /* add background image */
    this.background = new Background(assetsLoader.getTexture('assets/under_water.jpg'));
    this.background.width = 800;
    this.background.height = 600;
    //this.background.anchor.set(0.5);

    this.addChild(this.background);

    /* add fishes group animation */
    this.fishesGroup = this.addChild(new FishesGroup(0.2, assetsLoader.getTexture('assets/golden_fishes.png')));
    this.fishesGroup.createFishesGroup();

    this.fishesGroup = this.addChild(new FishesGroup(0.2, assetsLoader.getTexture('assets/school-of-fish.png')));
    this.fishesGroup.createFishesGroup();

    this.fishesGroup = this.addChild(new FishesGroup(0.1, assetsLoader.getTexture('assets/school-of-fish.png')));
    this.fishesGroup.createFishesGroup();

    this.fishesGroup = this.addChild(new FishesGroup(0.1, assetsLoader.getTexture('assets/school-of-fish.png')));
    this.fishesGroup.createFishesGroup();

    this.fishesGroup = this.addChild(new FishesGroup(0.1, assetsLoader.getTexture('assets/school-of-fish.png')));
    this.fishesGroup.createFishesGroup();

    this.gameController.idleState();

    /* add in game fishes container */
    this.fishesContainer = this.addChild(new PIXI.Container());
    this.fishesContainer.x = 0;
    this.fishesContainer.y = 0;

    /* add header */
    this.headerPanel = this.addChild(new Header());
    this.headerPanel.x = 0;
    this.headerPanel.y = 0;

    /* add counters */
    this.topCounter = this.addChild(new Counter());
    this.topCounter.x = 10;
    this.topCounter.y = 40;

    this.bottomCounter = this.addChild(new Counter());
    this.bottomCounter.x = 10;
    this.bottomCounter.y = 540;
  }

  initIDLEState () {
    if (this.startText) {
      this.removeChild(this.startText);
      this.startText = null;
    }
    /* add start button */
    this.startText = this.addChild(new PIXI.Text('Почати', BASE_STYLE));
    this.startText.x = (this.rootView.width - this.startText.width) * 0.5;
    this.startText.y = (this.rootView.height - this.startText.height) * 0.5;
    this.startText.interactive = true;
    this.startText.once('pointerdown', () => {
      this.headerPanel.reset();
      this.gameController.startGame();
    });
  }

  initInGameState () {
    if (this.startText) {
      this.removeChild(this.startText);
      this.startText.click = null;
      this.startText = null;
    }
    if (this.fishesContainer && this.fishesContainer.children.length) {
      this.fishesContainer.children.forEach(fish => {
        fish.stop();
      })
      this.fishesContainer.removeChildren();
    }
  }

  resizeHandler (width, height) {
    this.background && this.background.resize(width, height);
  }

  initGameHandler () {
    this.gameController.on(GAME_STATE.IDLE, () => {
      this.initIDLEState();
    });

    this.gameController.on(GAME_STATE.IN_GAME, () => {
      this.initInGameState();
    });

    this.gameController.on(GAME_STATE.GAME_OVER, () => {
      //this.initIDLEState();
    });

    this.gameController.on('success', () => {
      this.headerPanel.setSuccess();
    });

    this.gameController.on('wrong', () => {
      this.headerPanel.setWrong();
    });
  }

}

export default Game
