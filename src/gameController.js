import FISH_DATA, { FISH_POSITION, FISH_SIX } from './model/fish-data';
import SimpleFish from './view/components/simple-fish';
import { Howl } from 'howler';

const EventEmitter = require('events');

const GAME_STATE = {
  IDLE: 'idle',
  IN_GAME: 'inGame',
  WAITING: 'waiting',
  GAME_OVER: 'gameOver'
};


class GameController extends EventEmitter {
  constructor (gameInstance) {
    super()
    this.gameInstance = gameInstance;
    this.gameData = JSON.parse(JSON.stringify(FISH_DATA));
    this.state = GAME_STATE.IDLE;

    this.initSounds();
  }

  initSounds () {
    this.ambientSound = new Howl({
      src: ['assets/wave-41.mp3'],
      autoplay: true,
      loop: true,
      volume: 0.1,
    });

    this.click = new Howl(
      {
        src: ['assets/water-drop.wav'],
        autoplay: false,
        loop: false,
        volume: 0.5,
      }
    );

    this.clickWrong = new Howl(
      {
        src: ['assets/wrong-choice.wav'],
        autoplay: false,
        loop: false,
        volume: 0.5,
      }
    );
  }

  idleState () {
    this.state = GAME_STATE.IDLE;
    this.emit(GAME_STATE.IDLE);
  }

  startGame () {
    this.state = GAME_STATE.IN_GAME;
    this.emit(GAME_STATE.IN_GAME);

    this.gameData = JSON.parse(JSON.stringify(FISH_DATA));

    this.gameInstance.on('pointerdown', ({ data: { global } }) => {
      this.click.play();
      (this.state === GAME_STATE.IN_GAME) && this.checkFishPosition(global);
    });

    this.createBottomFish().then(() => {
      this.createNextFish();
    })
  }

  createBottomFish () {
    const fishList = [];

    for (let i = 0; i < 5; i++) {
      const fish = this.gameInstance.fishesContainer.addChild(new SimpleFish(FISH_SIX));

      if (FISH_SIX.position === FISH_POSITION.BOTTOM) {
        this.gameInstance.bottomCounter.inc();
      }

      fishList.push(fish.swimToPosition(true));
    }

    return Promise.all(fishList);
  }

  createNextFish () {
    if (!this.gameData.length) {
      this.gameOver();
      return;
    }
    const fishData = this.gameData.shift();
    this.currentFish = this.gameInstance.fishesContainer.addChild(new SimpleFish(fishData, fishData.texture || null))
    this.state = GAME_STATE.WAITING;
    setTimeout(() => {
      this.currentFish.swim().then(() => {
        this.state = GAME_STATE.IN_GAME;
      });
    }, 500);
  }

  checkFishPosition ({ y }) {
    if (this.currentFish) {
      if (this.currentFish.swimPosition === FISH_POSITION.TOP && y < 250 || this.currentFish.swimPosition === FISH_POSITION.BOTTOM && y > 350) {
        this.emit('success');
        this.state = GAME_STATE.WAITING;

        (this.currentFish.swimPosition === FISH_POSITION.BOTTOM) ? this.gameInstance.bottomCounter.inc() : this.gameInstance.topCounter.inc();

        this.currentFish.swimToPosition().then(() => {
          this.state = GAME_STATE.IN_GAME;
          this.createNextFish();
        });
      } else {
        this.clickWrong.play();
        this.emit('wrong');
      }

    }
  }

  gameOver () {
    this.emit(GAME_STATE.GAME_OVER);
    this.gameInstance.off('click');
  }
}

export { GAME_STATE }
export default GameController
