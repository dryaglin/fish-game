import * as PIXI from 'pixi.js';

const BASE_STYLE = new PIXI.TextStyle({
  fontFamily: 'Arial',
  fontSize: 36,
  fontWeight: 'bold',
  fill: ['#ffffff', '#00ff99'], // gradient
  stroke: '#4a1850',
  strokeThickness: 5,
  dropShadow: true,
  dropShadowColor: '#000000',
  dropShadowBlur: 4,
  dropShadowAngle: Math.PI / 6,
  dropShadowDistance: 6,
  wordWrap: true,
  wordWrapWidth: 440,
});

const FISH_NAME_STYLE = new PIXI.TextStyle({
  fontFamily: 'Arial',
  fontSize: 54,
  fontWeight: 'bold',
  fill: ['#ffffff'], // gradient
  dropShadow: true,
  dropShadowColor: '#000000',
  dropShadowBlur: 4,
  dropShadowAngle: Math.PI / 6,
  dropShadowDistance: 6,
  wordWrap: true,
  wordWrapWidth: 440,
});

const HEADER_TEXT = new PIXI.TextStyle({
  fontFamily: 'Arial',
  fontSize: 14,
  fontWeight: 'bold',
  fill: ['#ffffff'],
  wordWrap: false,
});

const COUNTER_TEXT = new PIXI.TextStyle({
  fontFamily: 'Arial',
  fontSize: 30,
  fontWeight: 'bold',
  fill: ['#ffffff'],
  wordWrap: false,
});

export {
  BASE_STYLE,
  FISH_NAME_STYLE,
  HEADER_TEXT,
  COUNTER_TEXT
};
