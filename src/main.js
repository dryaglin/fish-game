import * as PIXI from "pixi.js-legacy";
import './style/style.css'
import ASSETS from './model/assets';
import Game from './view/game';
import assetsLoader from './model/assets-loader';

class Main {
  constructor () {
    this.preInit();
    this.init();
    this.loadAssets();
  }

  preInit () {
    this.app = new PIXI.Application({
      width: 800,
      height: 600,
      antialias: true,
      transparent: false,
      resolution: 1,
    });

    document.body.appendChild(this.app.view);
  }

  init () {
    this.app.renderer.autoDensity = true;
    this.app.stage.interactive = true;

    window.onresize = () => {
      this._resizeHandler();
    }

    this._resizeHandler();
  }

  loadAssets () {
    assetsLoader.load(ASSETS).then(() => {
      this.onAssetsLoadedHandler();
    });
  }

  onAssetsLoadedHandler () {
    this.game = new Game();
    this.app.stage.addChild(this.game);
    this._resizeHandler();
  }

  _resizeHandler () {
    if (this.game) {
      this.game.resizeHandler(this.app.renderer.view.width, this.app.renderer.view.height);
    }
  }
}

export default new Main()
