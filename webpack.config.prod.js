const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default
const imageminMozjpeg = require('imagemin-mozjpeg')

module.exports = {
  entry: './src/main.js',
  mode: 'production',
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      title: 'Fish game'
    }),
    new CopyPlugin([
      { from: './assets', to: 'assets' },
    ]),
    new ImageminPlugin(
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        optipng: {
          optimizationLevel: 4
        },
        plugins: [
          imageminMozjpeg({
            quality: 70,
            progressive: true
          })
        ]
      }
    )
  ],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [{
      test: /\.css$/,
      use: ['style-loader', 'css-loader',]
    }]
  }
};
