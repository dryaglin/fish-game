const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: './src/main.js',
  mode: 'development',
  plugins: [
    /*new CleanWebpackPlugin(),*/
    new HtmlWebpackPlugin({
      title: 'Fish game'
    }),
    new CopyPlugin([
      { from: './assets', to: 'assets' },
    ])
  ],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [{
      test: /\.css$/,
      use: ['style-loader', 'css-loader',]
    }]
  }
};
